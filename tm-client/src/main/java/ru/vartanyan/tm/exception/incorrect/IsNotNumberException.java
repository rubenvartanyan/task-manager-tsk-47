package ru.vartanyan.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;

public class IsNotNumberException extends Exception{

    public IsNotNumberException(@NotNull final String number) throws Exception {
        super("Error! " + number + " is not number...");
    }

}
