package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.dto.*;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.api.service.model.IProjectTaskServiceGraph;
import ru.vartanyan.tm.api.service.model.ISessionServiceGraph;
import ru.vartanyan.tm.api.service.model.ITaskServiceGraph;

public interface ServiceLocator {

    @NotNull
    IProjectServiceGraph getProjectService();

    @NotNull
    IProjectTaskServiceGraph getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionServiceGraph getSessionService();

    @NotNull
    ITaskServiceGraph getTaskService();

    @NotNull
    ru.vartanyan.tm.api.service.model.IUserServiceGraph getUserService();

    @NotNull
    IProjectService getProjectServiceDTO();

    @NotNull
    IProjectTaskService getProjectTaskServiceDTO();

    @NotNull
    ISessionService getSessionServiceDTO();

    @NotNull
    ITaskService getTaskServiceDTO();

    @NotNull
    IUserService getUserServiceDTO();

    @NotNull
    IConnectionService getConnectionService();

}
