package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.dto.AbstractEntity;
import ru.vartanyan.tm.model.AbstractEntityGraph;

import javax.persistence.TypedQuery;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull E entity);

    E getSingleResult(@NotNull TypedQuery<E> query);

    void update(E entity);

}
