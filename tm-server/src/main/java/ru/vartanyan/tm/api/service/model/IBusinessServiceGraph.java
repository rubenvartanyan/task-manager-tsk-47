package ru.vartanyan.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.AbstractBusinessEntityGraph;

public interface IBusinessServiceGraph<E extends AbstractBusinessEntityGraph>
        extends IServiceGraph<E> {

    void changeStatusByIndex(
            @Nullable String userId, @Nullable Integer index, @Nullable Status status
    );

    void changeStatusByName(
            @Nullable String userId, @Nullable String name, @Nullable Status status
    );

    void finishById(@Nullable String userId, @Nullable String id);

    void finishByIndex(@Nullable String userId, @Nullable Integer index);

    void finishByName(@Nullable String userId, @Nullable String name);

    void startById(@Nullable String userId, @Nullable String id);

    void startByIndex(@Nullable String userId, @Nullable Integer index);

    void startByName(@Nullable String userId, @Nullable String name);

    @SneakyThrows
    void updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
