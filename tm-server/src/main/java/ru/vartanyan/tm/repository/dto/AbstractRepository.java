package ru.vartanyan.tm.repository.dto;

import org.hibernate.annotations.NaturalId;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.IRepository;
import ru.vartanyan.tm.dto.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    @Override
    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    @Nullable
    @Override
    public E getSingleResult(@NotNull final TypedQuery<E> query) {
        @Nullable final List<E> list = query.getResultList();
        if (list.size() != 0) {
            return list.get(0);
        }
        else {
            return null;
        }
    }

}

