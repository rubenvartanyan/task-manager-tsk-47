package ru.vartanyan.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.model.IProjectRepositoryGraph;
import ru.vartanyan.tm.api.repository.model.IUserRepositoryGraph;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.model.IProjectServiceGraph;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.ProjectGraph;
import ru.vartanyan.tm.repository.model.ProjectRepositoryGraph;
import ru.vartanyan.tm.repository.model.UserRepositoryGraph;


import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectServiceGraph extends AbstractServiceGraph<ProjectGraph>
        implements IProjectServiceGraph {

    public ProjectServiceGraph(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.add(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public ProjectGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepositoryGraph userRepository = new UserRepositoryGraph(entityManager);
            project.setUser(userRepository.findOneById(userId));
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            entities.forEach(projectRepository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(final @Nullable ProjectGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.removeOneById(entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @Nullable List<ProjectGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            return projectRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public @NotNull List<ProjectGraph> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            return projectRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }


    @SneakyThrows
    @Override
    public @Nullable ProjectGraph findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            return projectRepository.findOneByIdAndUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public @Nullable ProjectGraph findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            return projectRepository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }

    }

    @SneakyThrows
    @Override
    public @Nullable ProjectGraph findOneByName(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            return projectRepository.findOneByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void remove(
            @Nullable final String userId,
            @Nullable final ProjectGraph entity
    ) {
        if (userId == null) throw new EmptyIdException();
        if (entity == null) throw new NullObjectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.removeOneByIdAndUserId(userId, entity.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String userId, 
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.removeOneByIdAndUserId(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByIndex(
            @Nullable final String userId, 
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            @NotNull ProjectGraph project = projectRepository.findOneByIndex(userId, index);
            projectRepository.removeOneByIdAndUserId(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (status == null) throw new NullObjectException();
        @NotNull final ProjectGraph entity = findOneByName(userId, name);
        entity.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.COMPLETE);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId == null) throw new EmptyIdException();
        if (name == null) return;
        @NotNull final ProjectGraph entity = findOneByName(userId, name);
        entity.setStatus(Status.IN_PROGRESS);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (id == null) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneById(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null) throw new EmptyIdException();
        if (index == null || index == 0 || index < 0) throw new IncorrectIndexException();
        if (name == null) throw new EmptyNameException();
        @NotNull final ProjectGraph entity = findOneByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepositoryGraph projectRepository = new ProjectRepositoryGraph(entityManager);
            projectRepository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
